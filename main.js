$(document).ready(function () {
    var dados = [];

    $('#enviar').on('click', function () {
        let valida = true;
        $("form input").each(function () {
            let input = $(this);
            let nomes = input.prop('id');

            //Cancela o btn capture
            if (input.val() == '') {
                input.after('<span class="error">Preencha este campo</span>');
                $('.error').css('color', '#E10000C2');
                valida = false;
                //alert(`O campo ${input.prop('id')} nao pode estar vazio`);
            } else {
                input.css('background-color', '#4EE10068');
                $('.error').remove();
                if (nomes == 'telefone') {
                    if (input.val().length != 9) {
                        input.after('<span class="error">Preencha os 9 digitos pedidos</span>');
                        $('.error').css('color', '#E10000C2');
                        valida = false;
                    } else {
                        input.css('background-color', '#4EE10068');
                        $('.error').remove();
                    }
                }
            }
        });
        console.log(valida)
        let gravar = `
            <table>
                <tr>
                <td>Regina</td>
                <td>r@gmail.com</td>
                <td>21283837</td>
                <tr>
            </table>                
            `
        if (valida) {
            $('.cadastros_salvos').append(gravar);
            $('form')[0].reset();
            $('form input').css('background-color', '#fff');
        }
    });
});