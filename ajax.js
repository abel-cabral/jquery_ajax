$(document).ready(function () {
    let url = "https://icuff17-api.herokuapp.com/teachers";
   
    $('#enviar').click(function(){
        let id = $('#id').val();       
        $.ajax({
            url: `${url}/${id}`,
            method: 'GET',
            type: 'Json'
        }).done(function(data){
            $('#print h5').html(data.name);
            $('#print img').attr('src', data.photo);
        }).fail(function(data){
            if(data.status == 404){
                alert("Não existe esse ID cadastrado.");
            }else{
                alert(`Houve um erro na requisição ${data.status}`);
            }
        });
    
    });

});